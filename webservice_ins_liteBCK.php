<?php 
require_once('lib/nusoap.php');
include('conexion.inc.php') ;
$server = new soap_server();

$namespace = "http://avisosenprensa.com/webService/";

$server->configureWSDL("WebServiceINSLite",$namespace);
$server->wsdl->schemaTargetNamespace = $namespace;

$server->wsdl->addComplexType('Renovacion','complexType','struct','all','',
array(
'Name' => array('name'=>'Name','type'=>'xsd:string'),
'Mail' => array('name'=>'Mail','type'=>'xsd:string'),
'Password' => array('name'=>'Password','type'=>'xsd:string'),
'Tipo' => array('name'=>'Tipo','type'=>'xsd:int'),
'IdRunType' => array('name'=>'IdRunType','type'=>'xsd:int'),
'StartDate' => array('name'=>'StartDate','type'=>'xsd:dateTime'),
'StartTime' => array('name'=>'StartTime','type'=>'xsd:dateTime'),
'EndDate' => array('name'=>'EndDate','type'=>'xsd:dateTime'),
'EndTime' => array('name'=>'EndTime','type'=>'xsd:dateTime'),
'TimeInterval' => array('name'=>'TimeInterval','type'=>'xsd:int'),
'RunEveryDay' => array('name'=>'RunEveryDay','type'=>'xsd:boolean'),
'RunWeekDays' => array('name'=>'RunWeekDays','type'=>'xsd:boolean'),
'RunEveryXDays' => array('name'=>'RunEveryXDays','type'=>'xsd:int'),
'IntervaloRenovPago' => array('name'=>'IntervaloRenovPago','type'=>'xsd:int'),
'RenovarNocturno' => array('name'=>'RenovarNocturno','type'=>'xsd:int')));

$server->wsdl->addComplexType('RenovacionArray','complexType','array','','SOAP-ENC:Array',array(),array(array('ref'=>'SOAP-ENC:arrayType','wsdl:arrayType'=>'tns:Renovacion[]')),'tns:Renovacion');

//registrar la confirmacion de altualizacion
$server->register('IsClienteActivo',array('IdCliente'=>'xsd:int'),array('return' => 'xsd:boolean'), $ns);

$server->register('GetAnunciosActivos',array('IdCliente'=>'xsd:int'),array('return' => 'tns:RenovacionArray'), $ns);

$server->register('EnviarMail',array('IdCliente'=>'xsd:int','NombreAnuncio'=>'xsd:string'),array('return' => 'xsd:boolean'), $ns);

//envia los 
$POST_DATA = isset($GLOBALS['HTTP_RAW_POST_DATA'])
? $GLOBALS['HTTP_RAW_POST_DATA'] :'' ;
$server->service($POST_DATA);

exit();

//empiezan funciones
function IsClienteActivo($IdCliente)
{
	$link=conectar();
	$resulta = mysql_query("SELECT id_cliente FROM cliente WHERE id_cliente='".$IdCliente."' AND CURDATE() <= DATE(fecha_fin) ", $link);
	$cant = mysql_num_rows($resulta);
	
	desconectar();
		  
	return ($cant >0);
}
 
function GetAnunciosActivos($IdCliente)
{
	$renovacionList = array();

	$link=conectar();
	$resulta = mysql_query("SELECT * FROM cliente WHERE id_cliente='".$IdCliente."'  ", $link);//AND CURDATE() <= DATE(fecha_fin)
	if($row = mysql_fetch_assoc($resulta)){
		$Name = $row['nombre'];
		$Tipo = 0; // 0=>Renovacion 1=>RenovacionPago
		$IdRunType = 1;
		$StartDate = $row['fecha_inicio'];
		$StartTime = $row['ren_hora_inicio'];
		$EndDate = $row['fecha_fin'];
		$EndTime = $row['ren_hora_final'];
		$TimeInterval = $row['ren_int_tiempo'];
		$RunEveryDay = true;
		$RunWeekDays = true;
		$RunEveryXDays = 0;
		$IntervaloRenovPago = 0;
		$RenovarNocturno = 0;	

		$mails = explode(';', $row['ren_email']);
		$interval = $TimeInterval * count($mails);
		$i = 0;
		foreach ($mails as $item) {
			$temp = explode('|', $item);
			$Mail = $temp[0];
			$Password = $temp[1];
			
			$segundosAnadir = $i * $TimeInterval * 60; 
			$segundos_horaInicial=strtotime($StartTime); 
			$horainicial=date("H:i:s",$segundos_horaInicial+$segundosAnadir); 
			
			$renovacion = array();			
			$renovacion['Name'] = $Name;
			$renovacion['Mail'] = $Mail;
			$renovacion['Password'] = $Password;
			$renovacion['Tipo'] = $Tipo;
			$renovacion['IdRunType'] = $IdRunType;
			$renovacion['StartDate'] = $StartDate;
			$renovacion['StartTime'] = $horainicial;
			$renovacion['EndDate'] = $EndDate;
			$renovacion['EndTime'] = $EndTime;
			$renovacion['TimeInterval'] = $interval;
			$renovacion['RunEveryDay'] = $RunEveryDay;
			$renovacion['RunWeekDays'] = $RunWeekDays;
			$renovacion['RunEveryXDays'] = $RunEveryXDays;
			$renovacion['IntervaloRenovPago'] = $IntervaloRenovPago;
			$renovacion['RenovarNocturno'] = $RenovarNocturno;
		
			$renovacionList[] = $renovacion;
			$i = $i + 1;
		}
		
	} 
	
	desconectar();
		  
	return $renovacionList;
}
function EnviarMail($IdCliente, $NombreAnuncio)
{

	include('webservice_ins_lite_config.php') ;

	// Obtengo el cliente
	$link=conectar();
	$resulta = mysql_query("SELECT * FROM cliente WHERE id_cliente='".$IdCliente."' ", $link);
	if($row = mysql_fetch_assoc($resulta)){
		$cliente = $row['nombre'];
	} else {
		$cliente = 'No se encontro el cliente con id '.$IdCliente.'.';
		
	}
	desconectar();
	
	
	$destinatario = $config_mail['destinatario'];
	$asunto = "Error renovacion INS Lite";
	$cuerpo = '
	<html>
	<head>
	   <title>Prueba de correo</title>
	</head>
	<body>
	<h1>Fallo la renovacion del anuncio: '.$NombreAnuncio.' del cliente '.$cliente.'</h1>
	</body>
	</html>
	';

	//para el env�o en formato HTML
	$headers = "MIME-Version: 1.0\r\n";
	$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";

	//direcci�n del remitente
	$headers .= "From: INS Lite <".$config_mail['remitente'].">\r\n";

	$resultado = mail($destinatario, $asunto, $cuerpo, $headers);

	return ($resultado);
	
}
 
?>



