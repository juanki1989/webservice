<?php 
require_once('lib/nusoap.php');
include('conexion.inc.php') ;
//include('conexion.php') ;
$server = new soap_server();

$namespace = "http://avisosenprensa.com/webService/";

//$namespace = "http://localhost/webService/";
$server->configureWSDL("Actualizacioncliente",$namespace);
$server->wsdl->schemaTargetNamespace = $namespace;

//crea el tipo "obtenerConfirmacion"
$server->wsdl->addComplexType('ListaConfirmacion','complexType','struct','all','',
array('WebService' => array('name'=>'WebService','type'=>'xsd:int'),
'ID_Aviso' => array('name'=>'ID_Aviso','type'=>'xsd:int'),
'id_cliente' => array('name'=>'id_cliente','type'=>'xsd:int'),
'id_servidores' => array('name'=>'id_servidores','type'=>'xsd:int'),
'id_tablon' => array('name'=>'id_tablon','type'=>'xsd:int'),
'id_categoria' => array('name'=>'id_categoria','type'=>'xsd:int'),
'id_subCategoria' => array('name'=>'id_subCategoria','type'=>'xsd:int')));

$server->wsdl->addComplexType('ListaConfirmacionArray','complexType','array','','SOAP-ENC:Array',array(),array(array('ref'=>'SOAP-ENC:arrayType','wsdl:arrayType'=>'tns:ListaConfirmacion[]')),'tns:ListaConfirmacion');

$server->register('obtenerConfirmacion',array(),array('return'=>'tns:ListaConfirmacionArray'),$namespace,false,
'rpc',
false,
'devolucion lista de avisos pendientes');

//crea el tipo "obtenerConfirmacionPorMaquina"
$server->wsdl->addComplexType('ListaConfirmacionPC','complexType','struct','all','',
array('WebService' => array('name'=>'WebService','type'=>'xsd:int'),
'ID_Aviso' => array('name'=>'ID_Aviso','type'=>'xsd:int'),
'id_cliente' => array('name'=>'id_cliente','type'=>'xsd:int'),
'id_tablon' => array('name'=>'id_tablon','type'=>'xsd:int'),
'id_categoria' => array('name'=>'id_categoria','type'=>'xsd:int'),
'id_subCategoria' => array('name'=>'id_subCategoria','type'=>'xsd:int')));

$server->wsdl->addComplexType('ListaConfirmacionPCArray','complexType','array','','SOAP-ENC:Array',array(),
array(array('ref'=>'SOAP-ENC:arrayType','wsdl:arrayType'=>'tns:ListaConfirmacionPC[]')),'tns:ListaConfirmacionPC');

$server->register('obtenerConfirmacionPorMaquina',array('ID_servidores'=>'xsd:int'),array('return'=>'tns:ListaConfirmacionPCArray'),$namespace,false,
'rpc',
false,
'devolucion del id_cliente que tiene actualizaciones por maquina');
//crea el tipo ActualizacionCliente
//crea lista detralle

$server->wsdl->addComplexType('ListaAvisosDetalle','complexType','struct','all','',
array('id_detalle' => array('name'=>'id_detalle','type'=>'xsd:int'),
'id_cliente' => array('name'=>'id_cliente','type'=>'xsd:int'),
'id_tablon' => array('name'=>'id_tablon','type'=>'xsd:int'),
'id_campo' => array('name'=>'id_campo','type'=>'xsd:string'),
'nombre_campo' => array('name'=>'nombre_campo','type'=>'xsd:string'),
'nombre_tag' => array('name'=>'nombre_tag','type'=>'xsd:string'),
'id_campoTipo' => array('name'=>'id_campoTipo','type'=>'xsd:int'),
'valor' => array('name'=>'valor','type'=>'xsd:string')
));
//crea lista de variables imagen
$server->wsdl->addComplexType('ListaAvisosVarImagen','complexType','struct','all','',
array('id_detalleImagen' => array('name'=>'id_detalleImagen','type'=>'xsd:int'),
'ruta' => array('name'=>'ruta','type'=>'xsd:string'),
'activo' => array('name'=>'activo','type'=>'xsd:string')
));

//crea lista original
$server->wsdl->addComplexType('ListaAvisos','complexType','struct','all','',
array('id_cliente' => array('name'=>'id_cliente','type'=>'xsd:int'),
'id_Aviso' => array('name'=>'id_Aviso','type'=>'xsd:int'),
'nombre' => array('name'=>'nombre','type'=>'xsd:string'),
'url_web' => array('name'=>'url_web','type'=>'xsd:string'),
'estado' => array('name'=>'estado','type'=>'xsd:int'),
'hora_inicio' => array('name'=>'hora_inicio','type'=>'xsd:time'),
'hora_final' => array('name'=>'hora_final','type'=>'xsd:time'),
'fecha_inicio' => array('name'=>'fecha_inicio','type'=>'xsd:date'),
'fecha_fin' => array('name'=>'fecha_fin','type'=>'xsd:date'),
'int_tiempo' => array('name'=>'int_tiempo','type'=>'xsd:int'),
'restaurar_router' => array('name'=>'restaurar_router','type'=>'xsd:int'),
'nro_fotos' => array('name'=>'nro_fotos','type'=>'xsd:int'),
'conectar_vpn' => array('name'=>'conectar_vpn','type'=>'xsd:int'),
'id_tablon' => array('name'=>'id_tablon','type'=>'xsd:int'),
'orden_fotos' => array('name'=>'orden_fotos','type'=>'xsd:int'),
'orden_pub' => array('name'=>'orden_pub','type'=>'xsd:int'),
'orden_grup_pub' => array('name'=>'orden_grup_pub','type'=>'xsd:int'),
'modificarFoto' => array('name'=>'modificarFoto','type'=>'xsd:int'),
'textoFoto' => array('name'=>'textoFoto','type'=>'xsd:string'),
'enviar_foto' => array('name'=>'enviar_foto','type'=>'xsd:int'), //1=Si enviar ; 0=No enviar
'tipo_dias' => array('name'=>'tipo_dias','type'=>'xsd:int'),
'periodicidad_horaria' => array('name'=>'periodicidad_horaria','type'=>'xsd:int'),
'archivo' => array('name'=>'archivo','type'=>'xsd:string'),
'ListaDetalleAviso' => array('name'=>'ListaDetalleAviso','type'=>'tns:ListaAvisosDetalleArray'),
'nom_variableImagen' => array('name'=>'nom_variableImagen','type'=>'xsd:string'),
'ListaAvisosVarImagen' => array('name'=>'ListaAvisosVarImagen','type'=>'tns:ListaAvisosVarImagenArray')
));


$server->wsdl->addComplexType('ListaAvisosDetalleArray','complexType','array','','SOAP-ENC:Array',array(),
array(array('ref'=>'SOAP-ENC:arrayType','wsdl:arrayType'=>'tns:ListaAvisosDetalle[]')),'tns:ListaAvisosDetalle');

$server->wsdl->addComplexType('ListaAvisosVarImagenArray','complexType','array','','SOAP-ENC:Array',array(),
array(array('ref'=>'SOAP-ENC:arrayType','wsdl:arrayType'=>'tns:ListaAvisosVarImagen[]')),'tns:ListaAvisosVarImagen');

$server->wsdl->addComplexType('ListaAvisosArray','complexType','array','','SOAP-ENC:Array',array(),
array(array('ref'=>'SOAP-ENC:arrayType','wsdl:arrayType'=>'tns:ListaAvisos[]')),'tns:ListaAvisos');

$server->register('ListadoActualizacionClientes',array('ID_servidores'=>'xsd:int'),array('return'=>'tns:ListaAvisosArray')
,$namespace,false,
'rpc',
false,
'devolucion de listas de avisos especiales');
//registrar la confirmacion de altualizacion
$server->register('ActualizarWebservice',array('ID_servidores'=>'xsd:int','ID_Aviso'=>'xsd:int'),array('return' => 'xsd:string'),$ns);


//envia los 
$POST_DATA = isset($GLOBALS['HTTP_RAW_POST_DATA'])
? $GLOBALS['HTTP_RAW_POST_DATA'] :'' ;
$server->service($POST_DATA);

exit();
//empiezan funciones
//funcion que da la confirmacion  de nuevas actualizaciones  sin datos de entrada $per[0]['WebService']=0 si hay actualizaciones 
function obtenerConfirmacion()
 {
   $link=conectar();
 //  ($ID_servidores,$ID_tablon,$ID_categoria, $id_subcategoria) webservice =o , pendiente
   $result1 = mysql_query("select id_serv_cliente_tablon, id_cliente,id_servidores,id_tablon,WebService,id_categoria,id_subCategoria  from serv_cliente_tablon where WebService=0 ", $link);
   $numero=mysql_num_rows($result1);
   $i=0;
   if ($numero >0)
   { $respuesta = $ListaConfirmacion[$numero];
		 while ($row = mysql_fetch_array($result1))
	   {
		  $respuesta[$i]['WebService'] = $row["WebService"];
		  $respuesta[$i]['ID_Aviso'] = $row["id_serv_cliente_tablon"];
		  $respuesta[$i]['id_cliente'] = $row["id_cliente"];
		  $respuesta[$i]['id_servidores']= $row["id_servidores"];
		  $respuesta[$i]['id_tablon'] = $row["id_tablon"];
		  $respuesta[$i]['id_categoria'] = $row["id_categoria"];
		  $respuesta[$i]['id_subCategoria'] = $row["id_subCategoria"];
		   $i=$i+1;
	   } 
   }   
	else
   { 
      $respuesta = $ListaConfirmacion[2];
      $respuesta[0]['WebService'] = 1;
	  $respuesta[0]['ID_Aviso'] = 0;
	  $respuesta[0]['id_cliente'] = 20;
	  $respuesta[0]['id_servidores'] =0; 
	  $respuesta[0]['id_tablon'] = 0;
	  $respuesta[0]['id_categoria'] = 0;
	  $respuesta[0]['id_subCategoria'] = 0;
	 }
	  desconectar();
      $row->free;
	  return $respuesta; 
} 
//funcion que da la confirmacion  de nuevas actualizaciones por PC , $web=0 si hay actualizaciones , 4web si no hay actualizaciones
function obtenerConfirmacionPorMaquina($ID_servidores)
 {
    $linke=conectar();

 $result = mysql_query("select id_serv_cliente_tablon, id_cliente, id_servidores, id_tablon,WebService,id_categoria,id_subCategoria  from serv_cliente_tablon   where WebService=0 and id_servidores=$ID_servidores", $linke);
   
   $numero=mysql_num_rows($result);
   $i=0;
   
    if ($numero > 0)
   { $respuesta = $ListaConfirmacionPC[$numero];
		while ($row = mysql_fetch_array($result))
		{ 
		  $respuesta[$i]['WebService'] = $row["WebService"];
		  $respuesta[$i]['ID_Aviso'] = $row["id_serv_cliente_tablon"];
		  $respuesta[$i]['id_cliente'] = $row["id_cliente"];
		  $respuesta[$i]['id_tablon'] = $row["id_tablon"];
		  $respuesta[$i]['id_categoria'] = $row["id_categoria"];
		  $respuesta[$i]['id_subCategoria'] = $row["id_subCategoria"];
		  $i=$i+1;
	   } 
	}
	else
    { 
      $respuesta = $ListaConfirmacionPC[2];
      $respuesta[0]['WebService'] = 1;
	  $respuesta[0]['ID_Aviso'] = 0;
	  $respuesta[0]['id_cliente'] = 0;
	  $respuesta[0]['id_tablon'] = 0;
	  $respuesta[0]['id_categoria'] = 0;
	  $respuesta[0]['id_subCategoria'] = 0;
	 }
	    desconectar();
      $row1->free;
	  return $respuesta; 
} 
//funcion que saca las actualizaciones de los clientes
function ListadoActualizacionClientes($ID_servidores)
 {
   ini_set('memory_limit', '128M');
    $linkea=conectar();
  
$result = mysql_query("select C.modificarFotos, C.textoFotos , C.enviar_foto, C.id_cliente as id_cliente,C.carp_fotos, C.id_cliente as id_cliente, C.nombre as nombreCliente, C.id_categoria,S.id_tablon as id_tablon,P.url_web as url_web,C.estado as estado,CT.hora_inicio as hora_inicio, CT.hora_final as hora_final,C.fecha_inicio as fecha_inicio,C.fecha_fin as fecha_fin,CT.int_tiempo as int_tiempo,CT.restaurar_router as restaurar_router,C.nro_fotos as nro_fotos,CT.conectar_vpn as conectar_vpn,C.nom_archivo, S.id_serv_cliente_tablon,S.webService,C.orden_fotos, C.telefono,PR.orden_grupo as OrdenGrupoProv, PR.nombre as nomProvincia, P.nombre as nomTablon from cliente C, serv_cliente_tablon S, tablones P, cliente_tablon CT, provincias PR where C.id_cliente=S.id_cliente and C.id_cliente=CT.id_cliente and S.id_tablon=CT.id_tablon and PR.id_provincia=C.id_provincia and S.webService=0 and S.estado=1 and P.id_tablon=S.id_tablon and S.id_servidores=$ID_servidores Limit 0,2", $linkea);
 
   $numero=mysql_num_rows($result);
   $i=0;
   
    if ($numero > 0)
   { $lista = $ListaAvisos[$numero];
		while ($row = mysql_fetch_array($result))
		{ 
	   $var_IDcliente=$row["id_cliente"];
      $lista[$i]['id_cliente'] =$var_IDcliente;
	  
	  $var_IDtablon=$row["id_tablon"];
	  $lista[$i]['id_tablon'] = $var_IDtablon;
	  
	  $lista[$i]['id_Aviso'] = $row["id_serv_cliente_tablon"];
	  $lista[$i]['nombre'] = utf8_decode($row["nombreCliente"]." ".$row["telefono"]." - ".$row["nomProvincia"]." ".$row["nomTablon"]." (".$row["id_cliente"].")");
	  $lista[$i]['url_web'] = $row["url_web"];
	  $lista[$i]['estado'] = $row["estado"];
	  $lista[$i]['hora_inicio'] =$row["hora_inicio"];
	  
	  $hora_fin=$row["hora_final"];
     $lista[$i]['hora_final'] = $hora_fin;
	 
	  
	  $fecha_ini=$row["fecha_inicio"];
	  //$lista[$i]['fecha_inicio'] =$fecha_ini;                   
	  
	  $fecha_final=$row["fecha_fin"];
	  $lista[$i]['fecha_fin'] = $fecha_final;
	  $lista[$i]['int_tiempo'] = $row["int_tiempo"];
	  $lista[$i]['restaurar_router'] = $row["restaurar_router"];
      $lista[$i]['conectar_vpn'] = $row["conectar_vpn"];
	  $lista[$i]['nro_fotos'] = $row["nro_fotos"];
	  $lista[$i]['orden_fotos'] = $row["orden_fotos"];
	  $lista[$i]['orden_pub'] = 1;
	  $lista[$i]['orden_grup_pub'] = $row["OrdenGrupoProv"];
	  $lista[$i]['modificarFoto'] = $row["modificarFotos"];
	  $lista[$i]['textoFoto'] = $row["textoFotos"];
	  $lista[$i]['enviar_foto'] = 0;  //1=Si enviar ; 0=No enviar
	  //$tipo = 0 si es OneTime, $tipo=1 si es diary
	  //$periodicidad =0 si una vez  , $periodicidad =1 si es multiples veces
	  if ($fecha_final=='0000-00-00')
	  {$lista[$i]['fecha_fin']="2014-01-01";
	    $tipo=0;
	  }
	   else
	  {
	  $lista[$i]['fecha_fin']=$fecha_final;
	    $tipo=1;
	  }
	  if ($fecha_ini=='0000-00-00')
	  {
	  $lista[$i]['fecha_inicio']="2014-01-01";
	  }	  
	  else
	   {
	  $lista[$i]['fecha_inicio']=$fecha_ini;
	  }	
	  $lista[$i]['tipo'] = $tipo;
	   if ($hora_fin=='00:00:00')
	  {
	    $periodicidad=0;
	  }
	  else
	  {
	    $periodicidad=1;
	  }

      $lista[$i]['tipo_dias'] = $tipo;
	  $lista[$i]['periodicidad_horaria'] = $periodicidad;
      
	  $lista[$i]['webService'] = $row["webService"];
	  $lista[$i]['nom_variableImagen'] = $row["nom_archivo"];
	  
	  $fechaHoy = date("d-m-Y");
	  
	  if($row["enviar_foto"] == 1)
	  {
	  $archivo1=$row["id_cliente"].'/'.$row["nom_archivo"];
	  $nombre1='http://avisosenprensa.com/wac/logicaPresentacion/archivos_clientes/'.$archivo1.'.zip';
	  $lista[$i]['nombre1'] = file_get_contents($nombre1);
	  $lista[$i]['archivo'] = base64_encode(file_get_contents($nombre1));
	  }
	  else
	  {
	  $lista[$i]['nombre1'] = "sin_foto";
	  $lista[$i]['archivo'] = "sin_foto";
	  }
	  
	 //crea lista detalle
	  $result2=mysql_query("select id_cliente_tablon_campos,id_cliente,id_tablon,id_campo,nombre_campo,nombre_tag,id_campoTipo, valor  from cliente_tablon_campos where id_cliente=$var_IDcliente and id_tablon=$var_IDtablon order by id_cliente_tablon_campos asc");
		 $numDetalle=mysql_num_rows($result2);
         $j=0;
		 if($numDetalle >0)
		 {
            $detalle=$ListaAvisosDetalle[$numDetalle];
			while ($row2 = mysql_fetch_array($result2))
	       { 
		        $detalle[$j]['id_detalle'] = $row2["id_cliente_tablon_campos"];
				$detalle[$j]['id_cliente'] = $row2["id_cliente"];
				$detalle[$j]['id_tablon'] = $row2["id_tablon"];
				$detalle[$j]['id_campo'] = $row2["id_campo"];
				$detalle[$j]['nombre_campo'] = $row2["nombre_campo"];
				$detalle[$j]['nombre_tag'] = $row2["nombre_tag"];
				$detalle[$j]['id_campoTipo'] = $row2["id_campoTipo"];
				$detalle[$j]['valor'] = utf8_decode($row2["valor"]);
				$j=$j+1;
		   }
		 }
       else
		{
          $detalle=$ListaAvisosDetalle[2];
		  $detalle[$j]['id_detalle'] = 0;
		  $detalle[$j]['id_cliente'] = 0;
		  $detalle[$j]['id_tablon'] = 0;
		  $detalle[$j]['id_campo'] = 0;
		  $detalle[$j]['nombre_campo'] = '';
		  $detalle[$j]['nombre_tag'] = '';
		  $detalle[$j]['id_campoTipo'] = 0;
		  $detalle[$j]['valor'] = 0;
		
		 }
	 $lista[$i]['ListaDetalleAviso'] = $detalle;
	//  $row2->free;
	 //crea lista variable imagenes 
	 //empieza 
	  $resultado=mysql_query("SELECT id_archivo, ruta FROM archivos WHERE id_cliente=$var_IDcliente order by id_archivo asc");
		 $numDetalle=mysql_num_rows($resultado);
         $k=0;
		 if($numDetalle >0)
		 {
            $detalle_ima=$ListaAvisosVarImagen[$numDetalle];
			while ($row3 = mysql_fetch_array($resultado))
	       { 
		        $detalle_ima[$k]['id_detalleImagen'] = $row3["id_archivo"];
				$detalle_ima[$k]['ruta'] = $row3["ruta"];
				$detalle_ima[$k]['activo'] = 'True';
				$k=$k+1;
		   }
		 }
       else
		{
          $detalle_ima=$ListaAvisosVarImagen[1];
		  $detalle_ima[$k]['id_detalleImagen'] = 0;
		  $detalle_ima[$k]['ruta'] = '';
		  $detalle_ima[$k]['activo'] = '';
		 }
		$lista[$i]['ListaAvisosVarImagen'] = $detalle_ima;

	//  $row3->free;
	 //termina
	  $i=$i+1;
	   } 
	 }
	else
   { 
       $lista = $ListaAvisos[2];
      $lista[0]['id_cliente'] =0;
	  $lista[0]['id_Aviso'] = 0;
	  $lista[0]['nombre'] = '0';
	  $lista[0]['url_web'] = '0';
	  $lista[0]['estado'] = 0;
	  $lista[0]["hora_inicio"] ='00:00:00';
	  $lista[0]['hora_final'] ='00:00:00';
	  $lista[0]['fecha_inicio'] ="0001-01-01";
	  $lista[0]['fecha_fin'] = "0001-01-01";
	  $lista[0]['int_tiempo'] = 0;
	  $lista[0]['restaurar_router'] = 0;
	  $lista[0]['nro_fotos'] = 0;
	  $lista[0]['conectar_vpn'] = 0;
	  $lista[0]['id_tablon'] = 0;
	  $lista[0]['orden_fotos'] = 0;
	  $lista[0]['orden_pub'] = 0;
	  $lista[0]['orden_grup_pub'] = 0;
	  $lista[0]['enviar_foto'] = 0;
	  $lista[0]['tipo_dias'] = 0;
	  $lista[0]['periodicidad_horaria'] = 0;
	  $lista[0]['archivo'] = '0';
	  $lista[0]['modificarFoto'] = 0;
	  $lista[0]['textoFoto'] = '';
	 
	   $detalle=$ListaAvisosDetalle[1];
		  $detalle[0]['id_detalle'] = 0;
		  $detalle[0]['id_cliente'] = 0;
		  $detalle[0]['id_tablon'] = 0;
		  $detalle[0]['id_campo'] = 0;
		  $detalle[0]['nombre_campo'] = '';
		  $detalle[0]['nombre_tag'] = '';
		  $detalle[0]['id_campoTipo'] = 0;
		  $detalle[0]['valor'] = 0;
	  $lista[0]['ListaDetalleAviso'] = $detalle;
	   $detalle_ima=$ListaAvisosVarImagen[1];
		  $detalle_ima[0]['id_detalleImagen'] = 0;
		  $detalle_ima[0]['ruta'] = '';
		  $detalle_ima[0]['activo'] = '';
	  
	  $lista[0]['ListaAvisosVarImagen'] = $detalle_ima; 
	  $lista[0]['nom_variableImagen'] = '0';
	 }
	    desconectar();
        $row->free;
	  return $lista; 
} 
function ActualizarWebservice($Id_servidores, $Id_aviso)
{
 $linkea=conectar();
    $result = mysql_query("update serv_cliente_tablon set WebService=1 where id_servidores=$Id_servidores and id_serv_cliente_tablon=$Id_aviso", $linkea);
	 if ($result)
	 { 
		$respuesta ="Archivo Actualizado";	}
	else
	 { 		$respuesta ="No e pudo eliminar";	}
	 
	 return $respuesta;
} 
?>



